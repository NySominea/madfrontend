<?php

namespace App\Http\Controllers\CustomAuth;

use Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Client as HttpClient;

class RegisterController extends Controller
{
    public function showRegisterForm(){
        return view('auth.register');
    }

    public function register(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);

        if($validator->fails()){
            foreach($validator->errors()->messages() as $key => $error){
                $errors[$key] = reset($error);
            }
            return back()->withInput()->with('errors',$errors);      
        }
        
        $client =   new HttpClient();
        try{ 
            $response    =   $client->request('POST',  Config::get('constants.API_BASE_URL').'register', [
                'form_params'  => [
                    'email' => $request->email,
                    'name'  => $request->name,
                    'password' => $request->password,
                    'password_confirmation' => $request->password_confirmation
                ]
            ]);
            
            if($response->getStatusCode() === 200){
                $array_response   =   json_decode($response->getBody()->getContents(),true);
                
                return redirect()->route('login')->with('success',$array_response['message']);
            }

        }catch(BadResponseException $ex){
            $response = $ex->getResponse()->getBody();
            $errors   = json_decode($response,true);
            
            return back()->withInput()->with('errors',$errors['errorMessages']);
        }
        
    }
}
