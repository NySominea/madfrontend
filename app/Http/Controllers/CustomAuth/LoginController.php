<?php

namespace App\Http\Controllers\CustomAuth;

use Session;
use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Client as HttpClient;

class LoginController extends Controller
{
    public function showLoginForm(){
        return view('auth.login');
    }

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        if($validator->fails()){
            foreach($validator->errors()->messages() as $key => $error){
                $errors[$key] = reset($error);
            } 
            return back()->withInput()->withErrors($errors);      
        }

        $client = new HttpClient();

        try{
            $response    =   $client->request('POST', Config::get('constants.API_BASE_URL').'login', [
                'form_params'  => [
                    'email' => $request->email,
                    'password' => $request->password,
                    'remember' =>$request->remember
                ]
            ]);
            
            if($response->getStatusCode() === 200){
                $array_response = json_decode($response->getBody()->getContents(),true);
                
                Session::put('user',$array_response['data']['user']);
                Session::put('accessToken', $array_response['data']['accessToken']);
                return redirect()->route('user-homepage');
            }

        }catch(BadResponseException $ex){
            $statusCode = $ex->getResponse()->getStatusCode();
            $response   = $ex->getResponse()->getBody();
            $errors     = json_decode($response,true);
            
            if($statusCode == 403){
                return back()->withInput()->withError($errors['errorMessages'][0]);
            }
            elseif($statusCode == 401){
                return back()->withInput()->withError($errors['errorMessages'][0]);
            }
        }
    }

    public function logout(){
        $client = new HttpClient();

        try{
            $response    =   $client->request('POST',  Config::get('constants.API_BASE_URL').'logout', [
                'headers'   =>  [
                    'Accept'  =>  'application/json',
                    'Authorization' =>  'Bearer '.Session::get('accessToken')
                ]
            ]);
            
            if($response->getStatusCode() === 200){
                Session::flush();
                return redirect()->route('login');
            }
        }catch(BadResponseException $ex){
            return back();
        }
    }
}
