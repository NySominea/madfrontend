<?php

namespace App\Http\Controllers;
use Exception;
use Config;
use Session;
use Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ConnectException as ConnectException;
use Illuminate\Pagination\LengthAwarePaginator;

class FrontendController extends Controller
{
    public function index(){
        $client = new HttpClient();
        try{
            $response = $client->request('GET', 'https://jsonplaceholder.typicode.com/posts');                                                                                             
            
        }catch (ConnectException $e){
            abort(404); 
        }

        $response   =  $response->getBody()->getContents();
        
        $posts   =  json_decode($response);
        
        $posts  =   get_posts_pagination($posts, 6);
        
        return view('index',compact('posts'));
    }

    public function showPostsByAjax(){
        return view('ajax-posts');
    }

    public function getPostsByAjax(){
        $client = new HttpClient();
        
        try{
            $response = $client->request('GET', 'https://jsonplaceholder.typicode.com/posts');    
            
            $response   =  $response->getBody()->getContents();

            $posts   =  json_decode($response);
            
            
            return response()->json(['success' => true, 'data' => $posts]);

        }catch (ConnectException $e){
           
        }
        
    }

    public function home(){ 
        $client = new HttpClient();

        try{
            $response = $client->request('GET',  Config::get('constants.API_BASE_URL').'v1/posts',[
                'headers'   =>  [
                    'Accept'  =>  'application/json',
                    'Authorization' =>  'Bearer '.Session::get('accessToken')
                ],
            ]);
        }catch(BadResponseException $ex){
            $statusCode   =  $ex->getResponse()->getStatusCode();
            $response = $ex->getResponse()->getBody();
            $errors   = json_decode($response,true);
            
            if($statusCode == 401){
                Session::flush();
                return redirect()->route('login')->with('error',$errors['message']);
            }
        }
            
        $response   =  $response->getBody()->getContents();

        $posts   =  json_decode($response)->data;

        $posts  =   get_posts_pagination($posts, 6);
        
        return view('index',compact('posts'));

    }

    public function showAddPostForm(){
        return view('add-post');
    }

    public function savePost(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        
        if($validator->fails()){
            foreach($validator->errors()->messages() as $key => $error){
                $errors[$key] = reset($error);
            }
            return back()->withInput()->with('errors',$errors);      
        }
        
        $client =   new HttpClient();
        try{
            $response    =   $client->request('POST',  Config::get('constants.API_BASE_URL').'v1/post/save', [
                'headers'   =>  [
                    'Accept'  =>  'application/json',
                    'Authorization' =>  'Bearer '.Session::get('accessToken')
                ],
                'form_params'  => [
                    'title' => $request->title,
                    'body'  => $request->body,
                ]
            ]);
            
            if($response->getStatusCode() == 200){
                $array_response   =   json_decode($response->getBody()->getContents(),true);
                
                return back()->with('success',$array_response['message']);
            }

        }catch(BadResponseException $ex){
            $statusCode =  $ex->getResponse()->getStatusCode();
            $response = $ex->getResponse()->getBody();
            $errors   = json_decode($response,true);
            
            if($statusCode == 401){
                Session::flush();
                return redirect()->route('login')->with('error',$response['message']);
            }elseif($statusCode == 403){
                return back()->withInput()->with('errors',$errors['errorMessages']);
            }
        }
    }
}
