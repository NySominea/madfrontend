<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CustomAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(!Session::has('accessToken')){
            return redirect()->route('login');
        }
        return $next($request);
    }
}
