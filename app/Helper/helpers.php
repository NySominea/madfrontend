<?php
use Illuminate\Pagination\LengthAwarePaginator;

function get_posts_pagination($posts = [], $perPage = 0){

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    
    $itemCollection = collect($posts);

    $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

    $posts = new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);

    $posts->setPath(request()->url());
    
    return $posts;
}