<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses'  =>  'FrontendController@index',
    'as'    =>  'homepage'
]);

Route::get('/ajax/posts', [
    'uses'  =>  'FrontendController@showPostsByAjax',
    'as'    =>  'posts-by-ajax'
]);
Route::get('/ajax/get-posts', [
    'uses'  =>  'FrontendController@getPostsByAjax'
]);

Route::get('/regster',[
    'uses'  =>  'CustomAuth\RegisterController@showRegisterForm',
    'as'    =>  'show-register-form'
]);

Route::post('/regster',[
    'uses'  =>  'CustomAuth\RegisterController@register',
    'as'    =>  'register'
]);

Route::get('/login',[
    'uses'  =>  'CustomAuth\LoginController@showLoginForm',
    'as'    =>  'show-login-form'
]);

Route::post('/login',[
    'uses'  =>  'CustomAuth\LoginController@login',
    'as'    =>  'login'
]);


Route::group(['middleware' => 'custom.auth'], function(){
    Route::get('/home', [
        'uses'  =>  'FrontendController@home',
        'as'    =>  'user-homepage',
    ]);
    Route::get('/post/add', [
        'uses'  =>  'FrontendController@showAddPostForm',
        'as'    =>  'add-post',
    ]);
    Route::post('/post/add', [
        'uses'  =>  'FrontendController@savePost',
        'as'    =>  'save-post',
    ]);

    Route::get('/logout','CustomAuth\LoginController@logout');
    Route::post('/logout',[
        'uses'  =>  'CustomAuth\LoginController@logout',
        'as'    =>  'logout'
    ]);
});

