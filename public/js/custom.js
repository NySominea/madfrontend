$(document).ready(function(){
    var windowWidth =   $( window ).width();

    hidePaginationLink(windowWidth);
    $( window ).resize(function(){
        hidePaginationLink($(this).width());
    });
})

function hidePaginationLink(windowWidth){
    if(windowWidth < 430){
        $('ul.pagination li').hide().filter(':lt(1), :nth-last-child(1)').show();
    }else{
        $('ul.pagination li').show();
    }
}
