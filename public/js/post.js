$(document).ready(function(){
    var base_url  =   $('#base_url').val();
    var content   =   $('#post-list .row');
    
    $.ajax({
        type: "GET",
        url: base_url + '/ajax/get-posts',
        data: {},
        success: function( msg ) {
            var posts = msg.data;
            var image = base_url+'/assets/sample.jpg';
            var i;
            for(i = 0; i <= posts.length; i++ ){
                var data  = JSON.parse(JSON.stringify(posts[i]));
                var title = data.title;
                var body  = data.body;
                var post =  "<div class='pb-4 col-lg-4 col-md-6 col-sm-6 col-xs-12'>"+
                                "<div class='card w-100'>"+
                                    "<div class='img'><img class='card-img-top mw-100' src='"+base_url+"/assets/sample.jpg' alt=''></div>"+
                                    "<div class='card-body'><h4 class='card-title'>"+title+"</h4><p class='card-text'>"+body+"</p>"+
                                        "<div class='footer'> <div class='star-rating float-left'> <i class='fa fa-star'></i> <i class='fa fa-star'></i> <i class='fa fa-star'></i> <i class='fa fa-star'></i> <i class='fa fa-star-o'></i> </div> <div class='icons text-right'> <a href='#'><i class='fa fa-facebook'></i></a> <a href='#'><i class='fa fa-linkedin'></i></a> <a href='#'><i class='fa fa-dribbble'></i></a> <a href='#'><i class='fa fa-instagram'></i></a> </div> </div>"+    
                                    "</div>"+
                                "</div>"+
                            "</div>";
                
                content.append(post);
            };
        },
        error: function(){}
    });
});