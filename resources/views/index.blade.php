@extends('layouts.app')

@section('content')

    <div id="post-list" class="container pt-3">
        <h3>This load all posts using FrontendController.</h3>
        <div class="row">
            @if(isset($posts) && count($posts) > 0)
                @foreach($posts as $post)
                    <div class="pb-4 col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="card w-100">
                            <div class="img">
                                <img class="card-img-top mw-100" src="{{asset('assets/sample.jpg')}}" alt="{{$post->title}}">
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">{!! $post->title !!}</h4>
                                <p class="card-text">{!! $post->body !!}</p>
                                <div class="footer">
                                    <div class="star-rating float-left">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>
                                    <div class="icons text-right">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                        <a href="#"><i class="fa fa-dribbble"></i></a>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                No Posts
            @endif
        </div>
        @if(isset($posts) && count($posts) > 0 )
            {{ $posts->onEachSide(1)->links() }}
        @endif
    </div>
@endsection