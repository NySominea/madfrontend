<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.head')
</head>
<body>
    <div id="app">
        @include('layouts.header')

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <input type="hidden" id="base_url" name="base_url" value="{{url('/')}}">
</body>
    @include('layouts.scripts')
</html>
